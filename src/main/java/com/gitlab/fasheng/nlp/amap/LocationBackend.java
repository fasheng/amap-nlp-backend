/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

package com.gitlab.fasheng.nlp.amap;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Process;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import org.microg.nlp.api.LocationBackendService;
import org.microg.nlp.api.MPermissionHelperActivity;

import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
import com.amap.api.location.AMapLocationClientOption.AMapLocationProtocol;

import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.CHANGE_WIFI_STATE;
import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS;
import static android.Manifest.permission.BLUETOOTH;
import static android.Manifest.permission.BLUETOOTH_ADMIN;

public class LocationBackend extends LocationBackendService {

    private static final String TAG = "AMapNlpLocationBackend";

    public AMapLocationClient locationClient = null;
    private AMapLocationClientOption option = new AMapLocationClientOption();
    private LocationListener listener = new LocationListener();

    protected static LocationBackend instance;
    private boolean permsOkay = true;
    private static final String[] requiedPerms = new String[] {
        ACCESS_WIFI_STATE,
        CHANGE_WIFI_STATE,
        ACCESS_NETWORK_STATE,
        INTERNET,
        ACCESS_COARSE_LOCATION,
        ACCESS_FINE_LOCATION,
        WRITE_EXTERNAL_STORAGE,
        ACCESS_LOCATION_EXTRA_COMMANDS,
        BLUETOOTH,
        BLUETOOTH_ADMIN,
    };

    @Override
    public synchronized void onCreate() {
        super.onCreate();

        locationClient = new AMapLocationClient(getApplicationContext());
        locationClient.setLocationListener(listener);

        option.setLocationMode(AMapLocationMode.Battery_Saving);
        option.setInterval(10000); // 10s
        option.setMockEnable(true);
        option.setHttpTimeOut(20000);
        option.setLocationProtocol(AMapLocationProtocol.HTTPS);
        option.setNeedAddress(false);
        locationClient.setLocationOption(option);

        Log.d(TAG, "onCreate: use amap location sdk " + locationClient.getVersion());
    }

    @Override
    protected synchronized void onOpen() {
        super.onOpen();
        instance = this;

        permsOkay = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String s : requiedPerms) {
                permsOkay &= (checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED);
            }
        }
        if (permsOkay) {
            locationClient.startLocation();
            Log.d(TAG, "Activating instance at process " + Process.myPid());
        } else {
            Log.d(TAG, "onOpen: Permissions not granted");
        }
    }

    /**
     * Called by MicroG/UnifiedNlp when our backend is enabled. We return a list of
     * the Android permissions we need but have not (yet) been granted. MicroG will
     * handle putting up the dialog boxes, etc. to get our permissions granted.
     *
     * @return An intent with the list of permissions we need to run.
     */
    @Override
    protected Intent getInitIntent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Build list of permissions we need but have not been granted
            List<String> perms = new LinkedList<>();
            for (String s : requiedPerms) {
                if (checkSelfPermission(s) != PackageManager.PERMISSION_GRANTED)
                    perms.add(s);
            }

            // Send the list of permissions we need to UnifiedNlp so it can ask for
            // them to be granted.
            if (perms.isEmpty())
                return null;
            Intent intent = new Intent(this, MPermissionHelperActivity.class);
            intent.putExtra(MPermissionHelperActivity.EXTRA_PERMISSIONS, perms.toArray(new String[perms.size()]));
            return intent;
        }
        return super.getInitIntent();
    }

    @Override
    protected synchronized void onClose() {
        super.onClose();
        locationClient.stopLocation();
        if (instance == this) {
            instance = null;
            Log.d(TAG, "Deactivating instance at process " + Process.myPid());
        }
    }

    @Override
    protected synchronized Location update() {
        if (!permsOkay) {
            Log.d(TAG, "update: Permissions not granted");
        }
        return super.update();
    }
}
