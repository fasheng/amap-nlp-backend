/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

package com.gitlab.fasheng.nlp.amap;

import android.util.Log;
import android.location.Location;
import org.microg.nlp.api.LocationHelper;

import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.AMapLocation;

public class LocationListener implements AMapLocationListener {

    private static final String TAG = "AMapNlpLocationBackend";
    private static final String PROVIDER = "amap";

    @Override
    public void onLocationChanged(AMapLocation location) {
        if (location == null) {
            return;
        }

        if (location.getErrorCode() != 0) {
            // 定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
            Log.e(TAG, "location Error, ErrCode:"
                  + location.getErrorCode() + ", errInfo:"
                  + location.getErrorInfo());
            return;
        }

        int locType = location.getLocationType();
        if (locType == AMapLocation.LOCATION_TYPE_OFFLINE) {
            // 离线定位成功
            Log.d(TAG, "onLocationChanged: offline location success");
        }
        double latitude = location.getLatitude();   // 获取纬度信息
        double longitude = location.getLongitude(); // 获取经度信息
        float accuracy = location.getAccuracy();    // 获取定位精度，默认值为0.0f

        // convert gcj02 to wgs84
        if (location.getCoordType().equals(AMapLocation.COORD_TYPE_GCJ02)) {
            double []fixedLocation = Utils.gcj02towgs84(longitude, latitude);
            longitude = fixedLocation[0];
            latitude = fixedLocation[1];
        }

        Location response = LocationHelper.create(PROVIDER, latitude, longitude, accuracy);
        if (LocationBackend.instance != null) {
            Log.d(TAG, "response: locType=" + locType + " coorType=" + AMapLocation.COORD_TYPE_WGS84
                  + "(from " + location.getCoordType() + " " + location.getLatitude() + "," + location.getLongitude()
                  + ") locationDetail=" + location.getLocationDetail() + " " + response);
            LocationBackend.instance.report(response);
        }
    }
}
